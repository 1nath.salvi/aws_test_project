
terraform{
    backend "s3" {
        bucket = "eknaths3bucket"
        key = "terraform/terraform.tfstate"
        region = "us-east-1"
    }
}