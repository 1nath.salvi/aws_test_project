import boto3
import json
import requests
from botocore.exceptions import ClientError


def lambda_handler(event, context, contry):
    print("The get s3 bucket lambda is triggered")
    contents = dict()
    s3 = boto3.client('s3')
    try:
        data = s3.get_object(Bucket='eknaths3bucket', Key=f'{contry}_data.json')
    except ClientError as ex:
        if ex.response['Error']['Code'] == 'NoSuchKey':
            print(f'No data found for country {contry}')
            try:
                res = requests.get(fr"https://api.covid19api.com/country/{contry}")
                s3.put_object(Bucket='eknaths3bucket', Key=f'{contry}_data.json', Body=res.text)
            except Exception as E:
                print(f'Failed to create data for country {contry}')
                raise
            else:
                print("Created new json data file")

    else:
        contents = data['Body'].read().decode("utf-8")
        print(contents)

    return {
        'statusCode': 200,
        'body': json.dumps(contents)
    }


if __name__ == "__main__":
    event = {}
    context = {}
    lambda_handler(event, context, "india")
