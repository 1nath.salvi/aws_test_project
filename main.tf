provider "aws" {
  region = "us-east-1"
  version = "~> 2.60"
}

resource "aws_s3_bucket" "eknaths3bucket" {
  bucket = "eknaths3bucket"
  acl    = "private"

  tags = {
    App        = "AWS Sample"
  }
}

resource "aws_iam_role" "lamda_access_gitlab2" {
  name = "lamda_access_gitlab2"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_lambda_function" "main_lambda" {
  filename      = "build/${var.zip_file_name}"
  function_name = "main"
  role          = "${aws_iam_role.lamda_access_gitlab2.arn}"
  handler       = "main.lambda_handler"

  # The filebase64sha256() function is available in Terraform 0.11.12 and later
  # For Terraform 0.11.11 and earlier, use the base64sha256() function and the file() function:
  # source_code_hash = "${base64sha256(file("lambda_function_payload.zip"))}"

  runtime = "python3.7"
  timeout = "60"

}